package com.filez.sjs

import scala.scalajs.js

package object slouch {
   type doc = js.Dictionary[js.Object]
   type response = js.Dictionary[js.Any]
   type params = js.Dictionary[js.Any]
}