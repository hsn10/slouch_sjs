package com.filez.sjs.slouch

import scala.scalajs.js
import scala.scalajs.js.|
import scala.scalajs.js.annotation.JSImport


@JSImport("couch-slouch", JSImport.Namespace)
@js.native
class Slouch(url: String) extends js.Object {
   val attachment : Attachment = js.native
   val config : Config = js.native
   val db : DB = js.native
   val doc : Doc = js.native
   def ExcludeDesignDocIterator(iterator: js.Iterator[doc]) : js.Iterator[doc] = js.native
   val system : System = js.native
   val membership : Membership = js.native
   val NotAuthenticatedError : js.Function1[js.Any, Unit] = js.native
   val security : Security = js.native
   val user : User = js.native
   var forceReconnectAfterMilliseconds : Int = js.native
}

@JSImport("couch-slouch", JSImport.Namespace)
@js.native
object Slouch extends js.Object {
   val DEFAULT_FORCE_RECONNECT_AFTER_MILLISECONDS : Int = js.native
}

@js.native
sealed
trait Doc extends js.Object {
   var maxRetries : Int
   var ignoreDuplicateUpdates : Boolean
   def ignoreConflict[A](promiseFactory: js.Function0[js.Promise[A]]) : js.Promise[A]
   def isMissingError(err: js.Object) :  Boolean
   def isConflictError(err: js.Object) : Boolean
   def ignoreMissing[A](promiseFactory: js.Function0[js.Promise[A]]) : js.Promise[A]
   def create(dbName: String, doc: doc) : js.Promise[response]
   def createAndIgnoreConflict(dbName: String, doc: doc) : js.Promise[response]
   def update(dbName: String, doc: doc) : js.Promise[doc]
   def updateIgnoreConflict(dbName: String, doc: doc) : js.Promise[doc]
   def get(dbName: String, docId: String, params: params) : js.Promise[doc]
   def getIgnoreMissing(dbName: String, id: String) : js.Promise[doc]
   def exists(dbName: String, id: String) : js.Promise[Boolean]
   def updateOrIgnore(dbName:String, curDoc: doc, newDoc: doc) : js.Promise[doc]
   def createOrUpdate(dbname:String, doc: doc) : js.Promise[doc | response]
   def createOrUpdateIgnoreConflict(dbname: String, doc: doc) : js.Promise[doc | response]
   def upsert(dbName: String, doc: doc) : js.Promise[ doc | response ]
   def getMergeUpdate(dbname: String, doc: doc) : js.Promise[doc]
   def getMergeCreateOrUpdate(dbname: String, doc: doc) : js.Promise [ doc | response ]
   def getMergeUpdateIgnoreConflict(dbname: String, doc: doc) : js.Promise [doc]
   def getMergeUpsert(dbname: String, doc: doc) : js.Promise [ doc | response ]
   def getModifyUpsert(dbname: String, docId: String, onGetPromiseFactory: js.Function0[js.Promise[doc]]) : js.Promise[doc]
   def allArray(dbName: String, params: params) : js.Array[doc]
   def all(dbName: String, params: params) : js.Iterator[doc]
   def find(dbName: String, body: doc, params: params) : js.Promise[response]
   def destroyAllNonDesign(dbName: String) : js.Promise[doc]
   def destroyAll(dbName: String, keepDesignDoc: Boolean) : js.Promise[response]
   def destroy(dbName: String, docId: String, docRev: String) : js.Promise[response]
   def destroyIgnoreConflict(dbName: String, docId: String, docRev: String) : js.Promise[response]
   def getAndDestroy(dbName: String, docId: String) : js.Promise[response]
   def markAsDestroyed(dbName: String, docId: String) : js.Promise[doc]
   def setDestroyed(doc: doc) : doc
   def bulkCreateOrUpdate(dbName: String, docs: js.Array[doc]) : js.Promise[response]
}

@js.native
sealed
trait DB extends js.Object {
   def create(dbName: String) : js.Promise[Boolean]
   def destroy(dbName: String) : js.Promise[response]
   def get(dbName: String) : js.Promise[response]
   def exists(dbName: String) : js.Promise[Boolean]
   def changes(dbName: String, params: params, filter: String) : js.Iterator[response]
   def changesArray(dbName: String, params: params, filter: String) : js.Array[response]
   def view(dbName: String, viewDocId: String, view: String, params: params) : js.Iterator[response]
   def viewArray(dbName: String, viewDocId: String, view: String, params: params) : js.Array[response]
   def all() : js.Iterator[String]
   def replicate(params: params) : js.Promise[response]
   def copy(fromDBName: String, toDBName: String ) : js.Promise[response]
}

@js.native
sealed
trait Config extends js.Object {
   def get(path: String) : js.Promise[response]
   def set(path: String, value: response) : js.Promise[response]
   def unset(path: String) : js.Promise[response]
   def unsetIgnoreMissing(path: String) : js.Promise[response]
   def setCouchHttpdAuthTimeout(timeoutSecs : Int) : js.Promise[response]
   def setCouchHttpdAuthAllowPersistentCookies(allow : Boolean) : js.Promise[response]
   def setLogLevel(level: String) : js.Promise[response]
   def setCompactionRule(dbName: String, rule: String) : js.Promise[response]
   def setCouchDBMaxDBsOpen(maxDBsOpen: Int) : js.Promise[response]
   def setHttpdMaxConnections(maxConnections: Int) : js.Promise[response]
}

@js.native
sealed
trait Attachment extends js.Object {
   def get(dbName: String, docId: String, attachmentName: String) : js.Promise[js.Object]
   def destroy(dbName: String, docId: String, attachmentName: String, rev: String) : js.Promise[response]
}

@js.native
sealed
trait System extends js.Object {
   def isCouchDB1() : js.Promise[Boolean]
   def get() : js.Promise[response]
   def reset(exceptDBNames : Array[String] ) : js.Promise[Unit]
   def updates(params: params) : js.Iterator[response]
   def updatesViaGlobalChanges(params: params) : js.Iterator[response]
   def updatesNoHistory(params: params) : js.Iterator[response]
}

@js.native
sealed
trait Membership extends js.Object {
   def get() : js.Promise[response]
}

@js.native
sealed
trait Security extends js.Object {
   def set(dbName: String, security: js.Dynamic) : js.Promise[response]
   def get(dbName: String) : js.Promise[response]
   def onlyRoleCanView(dbName: String, role: String ) : js.Promise[response]
   def onlyUserCanView(dbName: String, user: String ) : js.Promise[response]
   def onlyAdminCanView(dbName: String) : js.Promise[response]
}

@js.native
sealed
trait User extends js.Object {
   def toUserId(username: String) : String
   def toUsername(userId: String) : String
   def create(username: String, password: String, roles: Array[String], metadata: Any) : js.Promise[response]
   def get(username: String, params: params) : js.Promise[response]
   def addRole(username: String, role: String) : js.Promise[response]
   def upsertRole(username: String, role: String) : js.Promise[response]
   def removeRole(username: String, role: String) : js.Promise[response]
   def downsertRole(username: String, role: String) : js.Promise[response]
   def setPassword(username: String, password: String) : js.Promise[response]
   def setMetadata(username: String, metadata: Any) : js.Promise[response]
   def destroy(username: String) : js.Promise[response]
   def authenticate(username: String, password: String) : js.Promise[response]
   def createSession(doc:doc) : js.Promise[response]
   def getSession(cookie: String, url: js.UndefOr[String]) : js.Promise[response]
   def destroySession(cookie: String) : js.Promise[response]
   def authenticated(cookie: String) : js.Promise[response]
   def authenticateAndGetSession(username: String, password: String) : js.Promise[js.Object]
   def setCookie(cookie: String) : Unit
   def logIn(username: String, password: String) : js.Promise[response]
   def logOut() : js.Promise[response]
}