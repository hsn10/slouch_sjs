package com.filez.sjs.slouch

import org.scalatest._
import org.scalatest.Matchers._

import scala.scalajs.js

// import scala.concurrent.ExecutionContext.Implicits.global
// import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.concurrent.Future
import scala.scalajs.js.Thenable.Implicits._

class UserSpec(url: String, admin: Tuple2[String,String]) extends AsyncWordSpec with MustMatchers {
   implicit override def executionContext = scala.concurrent.ExecutionContext.Implicits.global
   val s = new Slouch(url)

   "AdminUser" should {
      "scala future should run" in {
         Future { assert(true) }
      }
      "be able to log in" in {
         s.user.authenticate(admin._1, admin._2).map{ rc => println(s"**** $rc");assert(true) }

      }
    
   }

}