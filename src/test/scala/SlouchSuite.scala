package com.filez.sjs.slouch

import org.scalatest._
import org.scalatest.Matchers._

class SlouchSuite extends Suites() {
  val url    = "http://localhost:5984/"
  val dbName = "verifytestdb"
  val admin  = Tuple2("admin","admin")

  override val nestedSuites = Vector ( new UserSpec(url,admin) )
}